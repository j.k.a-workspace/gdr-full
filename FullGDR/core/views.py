from django.shortcuts import render,get_object_or_404

from django.views import generic
from django.db.models import Count
from django.urls import reverse_lazy
from .models import GrantGoal
from .forms import GrantGoalForm, UpdateGrantGoalForm
from django.http import HttpResponseRedirect
from .models import *
from .forms import *
from django.shortcuts import render
from django.views import View
from django.db import connection
from django.views.generic.edit import CreateView
from .models import Rutas



# Create your views here.

##### G R A N T G O A L C R U D #####

##Create
class CreateGrantGoal(generic.CreateView):
    template_name = "core/gg_create.html"
    model = GrantGoal
    form_class = GrantGoalForm
    success_url = reverse_lazy("core:gg_list")


## Retrieve
#List GrantGoal
class ListGrantGoal(generic.View):
    template_name = "core/gg_list.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = GrantGoal.objects.filter(status=True)
        self.context = {
            "grant_goals": queryset
        }
        return render(request, self.template_name, self.context)


#Detail
class DetailGrantGoal(generic.DetailView):
    template_name = "core/gg_detail.html"
    model = GrantGoal


#Update
class UpdateGrantGoal(generic.UpdateView):
    template_name = "core/gg_update.html"
    model = GrantGoal
    form_class = UpdateGrantGoalForm
    success_url = reverse_lazy("core:gg_list")


#Delete
class DeleteGrantGoal(generic.DeleteView):
    template_name = "core/gg_delete.html"
    model = GrantGoal
    success_url = reverse_lazy("core:gg_list")

#### R O U T E S C R U D ####

# Lista





class ListRoutes(generic.TemplateView):
    template_name = "core/list_routes.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ruta'] = Rutas.objects.values('noruta').distinct()
        return context
    
class DetailRoute(generic.ListView):
    template_name = "core/route_detail.html"
    model = Rutas
    context_object_name = 'ruta'

    def get_queryset(self):
        noruta = self.kwargs['noruta']
        return Rutas.objects.filter(noruta=noruta)
    
class CreateRoute(generic.CreateView):
    template_name = "core/route_create.html"
    model = Rutas
    form_class = CrearRuta
    success_url = reverse_lazy("core:list_routes")

class AddStop(generic.UpdateView):
    template_name = "core/route_stop.html"
    model = Rutas
    form_class = AddStop
    

    def get_success_url(self):
        return reverse_lazy('core:list_routes')

    def get_object(self, queryset=None):
        return Rutas.objects.get(pk=self.kwargs['numeroconsecutivo'])

class DeleteStop(generic.DeleteView):
    template_name = "core/route_stop_delete.html"
    model = Rutas
    success_url = reverse_lazy("core:list_routes")
  

class DeleteRoute(generic.DeleteView):
    template_name = "core/route_delete.html"
    model = Rutas
    success_url = reverse_lazy("core:list_routes")

    def get_object(self, queryset=None):
        routes = Rutas.objects.filter(noruta=self.kwargs['noruta'])
        return routes 
    
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        success_url = self.get_success_url()
        
        if self.object.exists():
            self.object.delete() 
            
        return HttpResponseRedirect(success_url)








class ListEmpleados(generic.TemplateView):
    template_name = "core/list_empleados.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['empleado'] = Empleados.objects.all()
        return context

class DetailEmpleados(generic.DetailView):
    template_name = "core/empleados_detail.html"
    model = Empleados
    context_object_name = 'empleado'
    success_url = reverse_lazy("core:list_empleados")

class CreateEmpleado(generic.CreateView):
    template_name = "core/empleados_create.html"
    model = Empleados
    form_class = CrearEmpleado
    success_url = reverse_lazy("core:list_empleados")

class UpdateEmpleado(generic.UpdateView):
    template_name = "core/empleados_update.html"
    model = Empleados
    form_class = UpdateEmpleado
    success_url = reverse_lazy("core:list_empleados")

class DeleteEmpleado(generic.DeleteView):
    template_name = "core/empleados_delete.html"
    model = Empleados
    success_url = reverse_lazy("core:list_empleados")

class ListConductores(generic.TemplateView):
    template_name = "core/list_conductores.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['conductor'] = Conductores.objects.all()
        return context

class DetailConductores(generic.DetailView):
    template_name = "core/conductores_detail.html"
    model = Conductores
    context_object_name = 'conductor'
    success_url = reverse_lazy("core:list_conductores")

class CreateConductores(generic.CreateView):
    template_name = "core/conductores_create.html"
    model = Conductores
    form_class = CreateConductor
    success_url = reverse_lazy("core:list_conductores")

class UpdateConductores(generic.UpdateView):
    template_name = "core/conductores_update.html"
    model = Conductores
    form_class = UpdateConductor
    success_url = reverse_lazy("core:list_conductores")

class DeleteConductores(generic.DeleteView):
    template_name = "core/conductores_delete.html"
    model = Conductores
    success_url = reverse_lazy("core:list_conductores")


class ListAdministradores(generic.TemplateView):
    template_name = "core/list_administradores.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['administra'] = Administrador.objects.all()
        return context

class DetailAdministrador(generic.DetailView):
    template_name = "core/administrador_detail.html"
    model = Administrador
    context_object_name = 'administra'
    success_url = reverse_lazy("core:list_administradores")

class CreateAdministrador(generic.CreateView):
    template_name = "core/administrador_create.html"
    model = Administrador
    form_class = CreateAdministrador
    success_url = reverse_lazy("core:list_administradores")

class UpdateAdministrador(generic.UpdateView):
    template_name = "core/administrador_update.html"
    model = Administrador
    form_class = UpdateAdministrador
    success_url = reverse_lazy("core:list_administradores")

class DeleteAdminstrador(generic.DeleteView):
    template_name = "core/administrador_delete.html"
    model = Administrador
    success_url = reverse_lazy("core:list_administradores")

class ListTransportes(generic.TemplateView):
    template_name = "core/list_transportes.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['transporte'] = Transportes.objects.all()
        return context

class DetailTrasporte(generic.DetailView):
    template_name = "core/transporte_detail.html"
    model = Transportes
    context_object_name = 'transporte'
    success_url = reverse_lazy("core:list_transportes")

class CreateTransporte(generic.CreateView):
    template_name = "core/transporte_create.html"
    model = Transportes
    form_class = CreateTransporte
    success_url = reverse_lazy("core:list_transportes")

class UpdateTransporte(generic.UpdateView):
    template_name = "core/transporte_update.html"
    model = Transportes
    form_class = UpdateTransporte
    success_url = reverse_lazy("core:list_transportes")

class DeleteTransporte(generic.DeleteView):
    template_name = "core/transporte_delete.html"
    model = Transportes
    success_url = reverse_lazy("core:list_transportes")


class ListParadas(generic.TemplateView):
    template_name = "core/list_paradas.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['parada'] = Paradas.objects.all()
        return context

class DetailParada(generic.DetailView):
    template_name = "core/parada_detail.html"
    model = Paradas
    context_object_name = 'parada'
    success_url = reverse_lazy("core:list_paradas")

class CreateParada(generic.CreateView):
    template_name = "core/parada_create.html"
    model = Paradas
    form_class = CreateParadas
    success_url = reverse_lazy("core:list_paradas")

class UpdateParada(generic.UpdateView):
    template_name = "core/parada_update.html"
    model = Paradas
    form_class = UpdateParadas
    success_url = reverse_lazy("core:list_paradas")

class DeleteParada(generic.DeleteView):
    template_name = "core/parada_delete.html"
    model = Paradas
    success_url = reverse_lazy("core:list_paradas")
    
#agregar empleados a un transporte

class ListEmpleadosTransportes(generic.TemplateView):
    template_name = "core/list_empleadostransportes.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['empleadotransporte'] = EmpleadosTransp.objects.all()
        return context
    
class CreateEmpleadoTransporte(generic.CreateView):
    template_name = "core/empleadotransporte_create.html"
    model = EmpleadosTransp
    form_class = CreateEmpleadoTransporte
    success_url = reverse_lazy("core:list_empleadostransportes")

class DeleteEmpleadoTransporte(generic.DeleteView):
    template_name = "core/empleadotransporte_delete.html"
    model = EmpleadosTransp
    success_url = reverse_lazy("core:list_empleadostransportes")

#agregar conductor a un transporte

class ListConductoresTransportes(generic.TemplateView):
    template_name = "core/list_conductorestransportes.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['conductortransporte'] = ConductoresTransp.objects.all()
        return context

class CreateConductorTransporte(generic.CreateView):
    template_name = "core/conductortransporte_create.html"
    model = ConductoresTransp
    form_class = CreateConductorTransporte
    success_url = reverse_lazy("core:list_conductorestransportes")
    
class DeleteConductorTransporte(generic.DeleteView):
    template_name = "core/conductortransporte_delete.html"
    model = ConductoresTransp
    success_url = reverse_lazy("core:list_conductorestransportes")


class NuevaTuplaRuta(CreateView):
    model = Rutas
    fields = ['numeroconsecutivo', 'noruta', 'conductor', 'transporte', 'parada', 'nombre', 'hora', 'fechainicio', 'fechafinal']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Obtener el ID de la tupla existente desde el contexto (en este ejemplo, asumiendo 'id_tupla' como el nombre del contexto)
        id_tupla_existente = self.kwargs['id_tupla']

        tupla_existente = Rutas.objects.get(numeroconsecutivo=id_tupla_existente)

        context['tupla_existente'] = tupla_existente
        return context

    def form_valid(self, form):
        # Obtener la tupla existente del contexto
        tupla_existente = self.get_context_data()['tupla_existente']

        # Obtener los valores que deseas mantener de la tupla existente
        noruta = tupla_existente.noruta
        conductor = tupla_existente.conductor
        transporte = tupla_existente.transporte

        # Obtener los valores actualizados del formulario
        nombre = form.cleaned_data['nombre']
        hora = form.cleaned_data['hora']
        fechainicio = form.cleaned_data['fechainicio']
        fechafinal = form.cleaned_data['fechafinal']

        # Crear una nueva tupla con los valores antiguos y nuevos
        nueva_tupla = Rutas.objects.create(noruta=noruta, conductor=conductor, transporte=transporte,
                                           nombre=nombre, hora=hora, fechainicio=fechainicio, fechafinal=fechafinal)
        return super().form_valid(form)